package de.neoskop.magnolia.dependencymanager.subapps;

import de.neoskop.magnolia.dependencymanager.presenter.OrphanedAssetsPresenter;
import de.neoskop.magnolia.dependencymanager.views.OrphanedAssetsView;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.framework.app.BaseSubApp;
import javax.inject.Inject;

public class OrphanedAssetsSubApp extends BaseSubApp<OrphanedAssetsView> {

  @Inject
  public OrphanedAssetsSubApp(SubAppContext subAppContext, OrphanedAssetsPresenter presenter) {
    super(subAppContext, presenter.start());
  }
}
