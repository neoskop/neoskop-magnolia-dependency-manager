package de.neoskop.magnolia.dependencymanager.subapps;

import de.neoskop.magnolia.dependencymanager.presenter.DependenciesPresenter;
import de.neoskop.magnolia.dependencymanager.views.DependenciesView;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.event.AdmincentralEventBus;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.contentapp.detail.DetailLocation;
import info.magnolia.ui.framework.app.BaseSubApp;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.contentconnector.JcrContentConnector;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.RepositoryException;

public class DependantAssetsSubApp extends BaseSubApp<DependenciesView> {
  private SimpleTranslator i18n;
  private EventBus adminCentralEventBus;
  private JcrContentConnector contentConnector;
  private final DependenciesPresenter presenter;
  private String caption;

  @Inject
  public DependantAssetsSubApp(
      SubAppContext subAppContext,
      SimpleTranslator i18n,
      @Named(AdmincentralEventBus.NAME) EventBus adminCentralEventBus,
      ContentConnector contentConnector,
      DependenciesPresenter presenter) {
    super(subAppContext, presenter.start());
    this.i18n = i18n;
    this.adminCentralEventBus = adminCentralEventBus;
    this.contentConnector = (JcrContentConnector) contentConnector;
    this.presenter = presenter;
    bindHandlers();
  }

  @Override
  public String getCaption() {
    return caption;
  }

  public void setCaption(String nodeName) {
    caption = i18n.translate("pages.dependencies.label", nodeName);
  }

  @Override
  public DependenciesView start(Location location) {
    super.start(location);
    presenter.setLocation(location);
    presenter.refresh();
    updateCaption(location);
    return getView();
  }

  @Override
  public void locationChanged(Location location) {
    presenter.setLocation(location);
    presenter.refresh();
    updateCaption(location);
  }

  @Override
  public DetailLocation getCurrentLocation() {
    return DetailLocation.wrap(super.getCurrentLocation());
  }

  @Override
  public boolean supportsLocation(Location location) {
    DetailLocation detailLocation = DetailLocation.wrap(location);
    DetailLocation currentDetailLocation = DetailLocation.wrap(getCurrentLocation());
    return currentDetailLocation.getNodePath().equals(detailLocation.getNodePath());
  }

  private void updateCaption(Location location) {
    DetailLocation detailLocation = DetailLocation.wrap(location);
    Object itemId = contentConnector.getItemIdByUrlFragment(detailLocation.getNodePath());

    try {
      setCaption(contentConnector.getItem(itemId).getJcrItem().getName());
    } catch (RepositoryException e) {
      e.printStackTrace();
    }
  }

  private void bindHandlers() {
    adminCentralEventBus.addHandler(
        ContentChangedEvent.class,
        event -> {
          // Check if the node still exist
          if (contentConnector.canHandleItem(event.getItemId())) {
            String currentNodePath = getCurrentLocation().getNodePath();
            JcrItemId itemId = contentConnector.getItemIdByUrlFragment(currentNodePath);
            JcrItemAdapter item = contentConnector.getItem(itemId);

            if (item == null) {
              getSubAppContext().close();
            }
          }
        });
  }
}
