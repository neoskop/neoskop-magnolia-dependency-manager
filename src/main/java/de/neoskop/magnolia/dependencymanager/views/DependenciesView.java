package de.neoskop.magnolia.dependencymanager.views;

import de.neoskop.magnolia.dependencymanager.util.NodeHolder;
import info.magnolia.ui.api.view.View;
import java.util.Map;
import java.util.Set;

public interface DependenciesView extends View {
  void setListener(Listener listener);

  void refresh(Map<NodeHolder, Set<NodeHolder>> dependencies);

  void refresh(Set<NodeHolder> dependencies);

  void setNodePath(String nodePath);

  interface Listener {
    void onFilterSubitemsChange(boolean filterSubitems);

    void onEdit(NodeHolder node);
  }
}
