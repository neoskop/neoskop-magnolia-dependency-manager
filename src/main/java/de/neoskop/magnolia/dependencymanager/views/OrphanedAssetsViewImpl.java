package de.neoskop.magnolia.dependencymanager.views;

import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Label;
import com.vaadin.v7.ui.Table;
import com.vaadin.v7.ui.TextField;
import com.vaadin.v7.ui.VerticalLayout;
import de.neoskop.magnolia.dependencymanager.domain.Filesize;
import info.magnolia.context.MgnlContext;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.ItemKey;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.ui.vaadin.actionbar.Actionbar;
import info.magnolia.ui.vaadin.grid.MagnoliaTable;
import info.magnolia.ui.vaadin.gwt.client.actionbar.shared.ActionbarItem;
import info.magnolia.ui.vaadin.icon.Icon;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

public class OrphanedAssetsViewImpl implements OrphanedAssetsView, Serializable {
  private static final long serialVersionUID = 1959126258247933086L;
  private static final String PATH_PROPERTY = "path";
  private static final String MIME_TYPE_PROPERTY = "mimetype";
  private static final String LAST_MODIFICATION_PROPERTY = "lastmod";
  private static final String FILESIZE_PROPERTY = "filesize";
  private final CssLayout searchBox = new CssLayout();
  private Listener listener;
  private HorizontalLayout layout = new HorizontalLayout();
  private Table table = new MagnoliaTable();
  private SimpleTranslator i18n;
  private Actionbar actionBar = new Actionbar();
  private CssLayout actionBarWrapper = new CssLayout();
  private VerticalLayout workbench = new VerticalLayout();
  private TextField searchField;
  private Button clearSearchBoxButton;
  private Icon searchIcon;
  private Icon searchArrow;
  private CssLayout toolBar = new CssLayout();

  @Inject
  public OrphanedAssetsViewImpl(SimpleTranslator i18n) {
    this.i18n = i18n;

    table.addContainerProperty(PATH_PROPERTY, Label.class, null);
    table.setColumnExpandRatio(PATH_PROPERTY, 0.70f);
    table.setColumnHeader(PATH_PROPERTY, i18n.translate("orphanedassets.app.headers.path"));

    table.addContainerProperty(MIME_TYPE_PROPERTY, String.class, null);
    table.setColumnExpandRatio(MIME_TYPE_PROPERTY, 0.10f);
    table.setColumnHeader(
        MIME_TYPE_PROPERTY, i18n.translate("orphanedassets.app.headers.mimetype"));

    table.addContainerProperty(FILESIZE_PROPERTY, Filesize.class, null);
    table.setColumnExpandRatio(FILESIZE_PROPERTY, 0.10f);
    table.setColumnHeader(FILESIZE_PROPERTY, i18n.translate("orphanedassets.app.headers.filesize"));

    table.addContainerProperty(LAST_MODIFICATION_PROPERTY, Date.class, null);
    table.setColumnExpandRatio(LAST_MODIFICATION_PROPERTY, 0.10f);
    table.setColumnHeader(
        LAST_MODIFICATION_PROPERTY, i18n.translate("orphanedassets.app.headers.lastmod"));

    table.setSortContainerPropertyId(PATH_PROPERTY);
    table.setSortAscending(true);

    table.setSizeFull();
    table.setImmediate(true);
    table.setSelectable(true);
    table.setMultiSelect(true);
    table.setNullSelectionAllowed(true);

    table.setCellStyleGenerator(
        new Table.CellStyleGenerator() {
          @Override
          public String getStyle(Table source, Object itemId, Object propertyId) {
            if (propertyId == null && itemId != null) {
              final Item item = source.getContainerDataSource().getItem(itemId);
              final ItemKey itemKey =
                  (ItemKey) ((Label) item.getItemProperty(PATH_PROPERTY).getValue()).getData();
              return listener.getIcon(itemKey);
            }
            return null;
          }
        });

    clearSearchBoxButton = new Button();
    clearSearchBoxButton.setStyleName("m-closebutton");
    clearSearchBoxButton.addStyleName("icon-delete-search");
    clearSearchBoxButton.addStyleName("searchbox-clearbutton");
    clearSearchBoxButton.addClickListener(event -> searchField.setValue(""));

    searchIcon = new Icon("search");
    searchIcon.addStyleName("searchbox-icon");

    searchArrow = new Icon("arrow2_s");
    searchArrow.addStyleName("searchbox-arrow");

    searchField = buildSearchField();

    searchBox.setVisible(true);
    searchBox.addComponent(searchField);
    searchBox.addComponent(clearSearchBoxButton);
    searchBox.addComponent(searchIcon);
    searchBox.addComponent(searchArrow);
    searchBox.setStyleName("searchbox");

    toolBar.addStyleName("toolbar");
    toolBar.setWidth(100, Unit.PERCENTAGE);
    toolBar.addComponent(searchBox);

    workbench.addComponent(toolBar);
    workbench.setExpandRatio(toolBar, 0);
    workbench.addComponent(table);
    workbench.setExpandRatio(table, 1);
    workbench.setHeight("100%");
    workbench.addStyleName("workbench");

    actionBar.addSection("main", i18n.translate("orphanedassets.app.actionBarSections.main"));
    actionBar.addAction(
        new ActionbarItem(
            "refresh",
            i18n.translate("orphanedassets.app.actions.refresh"),
            "icon-rotate-image-cw",
            "main"),
        "main");
    actionBar.addAction(
        new ActionbarItem(
            "remove", i18n.translate("orphanedassets.app.actions.remove"), "icon-delete", "remove"),
        "main");
    actionBar.addAction(
        new ActionbarItem(
            "publishDeletion",
            i18n.translate("orphanedassets.app.actions.publishDeletion"),
            "icon-publish",
            "remove"),
        "main");
    actionBar.addAction(
        new ActionbarItem(
            "edit", i18n.translate("orphanedassets.app.actions.edit"), "icon-edit", "asset"),
        "main");
    actionBar.setActionEnabled("remove", false);
    actionBar.setActionEnabled("edit", false);
    actionBar.setActionEnabled("publishDeletion", false);

    actionBarWrapper.setHeight(100, Unit.PERCENTAGE);
    actionBarWrapper.addStyleName("actionbar");
    actionBarWrapper.addComponent(actionBar);

    layout.addStyleName("browser");
    layout.setSizeFull();
    layout.setMargin(false);
    layout.setSpacing(true);
    layout.addComponent(workbench);
    layout.setExpandRatio(workbench, 1);

    layout.addComponent(actionBarWrapper);
    layout.setExpandRatio(actionBarWrapper, 0);

    addStyles();
    bindHandlers();
  }

  private TextField buildSearchField() {
    final TextField field = new TextField();
    field.setInputPrompt(i18n.translate("toolbar.search.prompt"));
    field.setSizeUndefined();
    field.addStyleName("searchfield");
    field.setImmediate(true);
    field.addValueChangeListener(event -> listener.onSearch(field.getValue()));
    return field;
  }

  private void addStyles() {
    Styles styles = Page.getCurrent().getStyles();
    styles.add(".workbench { padding: 20px 0 40px 40px; }");
  }

  private void bindHandlers() {

    table.addValueChangeListener(
        event -> {
          Object value = event.getProperty().getValue();

          if (listener != null) {
            Set<Object> items;

            if (value instanceof Set) {
              items = (Set) value;
            } else if (value == null) {
              items = Collections.emptySet();
            } else {
              items = new LinkedHashSet<>();
              items.add(value);
            }

            Set<ItemKey> assetKeys =
                items
                    .stream()
                    .map(
                        i ->
                            (ItemKey)
                                ((Label) table.getItem(i).getItemProperty(PATH_PROPERTY).getValue())
                                    .getData())
                    .collect(Collectors.toSet());
            listener.onItemSelection(assetKeys);

            List<String> nodePaths = new ArrayList<>();
            Iterator<Object> iterator = items.iterator();
            while (iterator.hasNext()) {
              Item tableItem = table.getItem(iterator.next());
              nodePaths.add(((Label) tableItem.getItemProperty("path").getValue()).getValue());
            }

            boolean isOneNodeMarkedAsDeleted = isOneNodeMarkedAsDeleted(nodePaths);
            boolean isOneNodeNotMarkedAsDeleted = isOneNodeNotMarkedAsDeleted(nodePaths);

            actionBar.setActionEnabled("remove", !items.isEmpty() && isOneNodeNotMarkedAsDeleted);
            actionBar.setActionEnabled("edit", items.size() == 1);
            actionBar.setActionEnabled(
                "publishDeletion", !items.isEmpty() && isOneNodeMarkedAsDeleted);
          }
        });

    actionBar.addActionTriggerListener(
        event -> {
          switch (event.getActionName()) {
            case "main:refresh":
              listener.onRefresh();
              break;

            case "main:remove":
              listener.onMarkDelete();
              break;

            case "main:edit":
              listener.onEdit();
              break;

            case "main:publishDeletion":
              listener.onPublishDeletion();
              break;
          }
        });

    table.addItemClickListener(
        e -> {
          if (e.isDoubleClick()) {
            listener.onEdit();
          }
        });
  }

  private boolean isOneNodeNotMarkedAsDeleted(List<String> nodePaths) {
    return nodePaths
        .stream()
        .filter(path -> isNodeNotMarkedAsDeleted(path))
        .findFirst()
        .isPresent();
  }

  private boolean isNodeNotMarkedAsDeleted(String nodePath) {
    if (nodePath != null) {
      try {
        Session session = MgnlContext.getInstance().getJCRSession(DamConstants.WORKSPACE);
        Node node = session.getNode(nodePath);
        return PropertyUtil.getDate(node, "mgnl:deleted") == null;
      } catch (RepositoryException e) {
      }
    }

    return false;
  }

  private boolean isOneNodeMarkedAsDeleted(List<String> nodePaths) {
    return nodePaths.stream().filter(path -> isNodeMarkedAsDeleted(path)).findFirst().isPresent();
  }

  private boolean isNodeMarkedAsDeleted(String nodePath) {
    if (nodePath != null) {
      try {
        Session session = MgnlContext.getInstance().getJCRSession(DamConstants.WORKSPACE);
        Node node = session.getNode(nodePath);
        return PropertyUtil.getDate(node, "mgnl:deleted") != null;
      } catch (RepositoryException e) {
      }
    }

    return false;
  }

  @Override
  public Component asVaadinComponent() {
    return layout;
  }

  @Override
  public void setListener(Listener listener) {
    this.listener = listener;
  }

  @Override
  public void refresh(List<Asset> retrieveOrphanedAssets) {
    table.removeAllItems();

    retrieveOrphanedAssets
        .stream()
        .forEach(
            a -> {
              Object newItemId = table.addItem();
              Item newRow = table.getItem(newItemId);
              Label pathLabel = new Label(a.getPath());
              pathLabel.setData(a.getItemKey());
              newRow.getItemProperty(PATH_PROPERTY).setValue(pathLabel);
              newRow.getItemProperty(MIME_TYPE_PROPERTY).setValue(a.getMimeType());
              newRow.getItemProperty(FILESIZE_PROPERTY).setValue(new Filesize(a.getFileSize()));
              newRow
                  .getItemProperty(LAST_MODIFICATION_PROPERTY)
                  .setValue(a.getLastModified().getTime());
            });
  }
}
