package de.neoskop.magnolia.dependencymanager.views;

import com.google.common.collect.Sets;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Component;
import com.vaadin.v7.data.Item;
import com.vaadin.v7.ui.CheckBox;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Label;
import com.vaadin.v7.ui.VerticalLayout;
import de.neoskop.magnolia.dependencymanager.util.NodeHolder;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.vaadin.grid.MagnoliaTable;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.jcr.RepositoryException;

public class DependenciesViewImpl implements DependenciesView {
  private static final long serialVersionUID = 520774958678054473L;
  private static final String PATH_PROPERTY = "path";
  private static final String PAGES_PROPERTY = "pages";

  private VerticalLayout layout = new VerticalLayout();
  private HorizontalLayout toolbar = new HorizontalLayout();
  private MagnoliaTable table = new MagnoliaTable();
  private CheckBox checkbox = new CheckBox();
  private String nodePath;
  private Listener listener;

  @Inject
  public DependenciesViewImpl(SimpleTranslator i18n) {
    layout.setSizeFull();
    layout.addStyleName("workbench");
    layout.setMargin(true);
    layout.setSpacing(true);

    table.addContainerProperty(PATH_PROPERTY, Label.class, null);
    table.addContainerProperty(PAGES_PROPERTY, String.class, null);

    table.setColumnExpandRatio(PATH_PROPERTY, 1);
    table.setColumnExpandRatio(PAGES_PROPERTY, 1);
    table.setSizeFull();
    table.setSelectable(false);

    table.addItemClickListener(
        e -> {
          if (e.isDoubleClick()) {
            NodeHolder node =
                (NodeHolder)
                    ((Label) e.getItem().getItemProperty(PATH_PROPERTY).getValue()).getData();
            listener.onEdit(node);
          }
        });

    table.setCellStyleGenerator(
        (source, itemId, propertyId) -> {
          if (propertyId == null && itemId != null) {
            return "icon-file-webpage";
          }
          return null;
        });

    table.setColumnHeader(PATH_PROPERTY, i18n.translate("pages.dependencies.headers.path"));
    table.setColumnHeader(PAGES_PROPERTY, i18n.translate("pages.dependencies.headers.pages"));

    checkbox.addValueChangeListener(e -> listener.onFilterSubitemsChange(checkbox.getValue()));
    checkbox.setCaption(i18n.translate("dependencies.includesubitems"));

    toolbar.setWidth(100, Unit.PERCENTAGE);
    toolbar.addComponent(checkbox);

    layout.addComponent(toolbar);
    layout.setExpandRatio(toolbar, 0);
    layout.addComponent(table);
    layout.setExpandRatio(table, 1);
  }

  @Override
  public void refresh(Map<NodeHolder, Set<NodeHolder>> dependencies) {
    table.removeAllItems();

    for (NodeHolder node : dependencies.keySet()) {
      try {
        Object newItemId = table.addItem();
        Item newRow = table.getItem(newItemId);
        Label label = new Label(node.toString());
        label.setData(node);
        newRow.getItemProperty(PATH_PROPERTY).setValue(label);

        Set<String> pageNames = Sets.newHashSet();

        for (NodeHolder page : dependencies.get(node)) {
          pageNames.add(page.getNode().getName());
        }

        newRow.getItemProperty(PAGES_PROPERTY).setValue(String.join(", ", pageNames));
      } catch (UnsupportedOperationException | RepositoryException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void refresh(Set<NodeHolder> dependencies) {
    layout.removeComponent(toolbar);
    table.setVisibleColumns(PATH_PROPERTY);
    table.removeAllItems();

    for (NodeHolder node : dependencies) {
      try {
        Object newItemId = table.addItem();
        Item newRow = table.getItem(newItemId);
        Label label = new Label(node.toString());
        label.setData(node);
        newRow.getItemProperty(PATH_PROPERTY).setValue(label);
      } catch (UnsupportedOperationException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public Component asVaadinComponent() {
    return layout;
  }

  public String getNodePath() {
    return nodePath;
  }

  public void setNodePath(String nodePath) {
    this.nodePath = nodePath;
  }

  @Override
  public void setListener(Listener listener) {
    this.listener = listener;
  }
}
