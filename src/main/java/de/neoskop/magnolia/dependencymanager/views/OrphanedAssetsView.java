package de.neoskop.magnolia.dependencymanager.views;

import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.ItemKey;
import info.magnolia.ui.api.view.View;
import java.util.List;
import java.util.Set;

public interface OrphanedAssetsView extends View {

  void setListener(Listener listener);

  void refresh(List<Asset> retrieveOrphanedAssets);

  interface Listener {
    void onItemSelection(Set<ItemKey> assetUuids);

    void onRefresh();

    String getIcon(ItemKey itemKey);

    void onSearch(String value);

    void onMarkDelete();

    void onEdit();

    void onPublishDeletion();
  }
}
