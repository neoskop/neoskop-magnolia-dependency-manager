package de.neoskop.magnolia.dependencymanager.presenter;

import de.neoskop.magnolia.dependencymanager.services.ContentDependencyService;
import de.neoskop.magnolia.dependencymanager.util.NodeHolder;
import de.neoskop.magnolia.dependencymanager.views.DependenciesView;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.api.location.LocationController;
import info.magnolia.ui.contentapp.detail.DetailLocation;
import info.magnolia.ui.contentapp.detail.DetailView;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.contentconnector.JcrContentConnector;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class DependenciesPresenter implements DependenciesView.Listener {
  private final DependenciesView view;
  private final LocationController locationController;
  private final ContentDependencyService contentDependencyService;
  private final JcrContentConnector contentConnector;
  private boolean filterSubitems = false;
  private Location location;

  @Inject
  public DependenciesPresenter(
      DependenciesView view,
      LocationController locationController,
      ContentDependencyService contentDependencyService,
      ContentConnector contentConnector) {
    this.view = view;
    this.locationController = locationController;
    this.contentDependencyService = contentDependencyService;
    this.contentConnector = (JcrContentConnector) contentConnector;
  }

  public DependenciesView start() {
    view.setListener(this);
    return view;
  }

  public void refresh() {
    DetailLocation detailLocation = DetailLocation.wrap(location);
    view.setNodePath(detailLocation.getNodePath());

    try {
      Object itemId = contentConnector.getItemIdByUrlFragment(detailLocation.getNodePath());
      Node node = (Node) contentConnector.getItem(itemId).getJcrItem();

      if (node.isNodeType(NodeTypes.Page.NAME)) {
        Map<NodeHolder, Set<NodeHolder>> dependencies =
            contentDependencyService.retrieveDependentResourcesOfPage(
                detailLocation.getNodePath(), filterSubitems);
        view.refresh(dependencies);

      } else {
        Set<NodeHolder> dependencies =
            contentDependencyService.retrieveDependentResourcesOfAsset(
                detailLocation.getNodePath());
        view.refresh(dependencies);
      }
    } catch (RepositoryException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onFilterSubitemsChange(boolean filterSubitems) {
    this.filterSubitems = filterSubitems;
    refresh();
  }

  @Override
  public void onEdit(NodeHolder node) {
    String appName = node.isPage() ? "pages" : "assets";
    DetailLocation targetLocation =
        new DetailLocation(appName, "detail", DetailView.ViewType.EDIT, node.getPath(), "");
    locationController.goTo(targetLocation);
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Location getLocation() {
    return location;
  }
}
