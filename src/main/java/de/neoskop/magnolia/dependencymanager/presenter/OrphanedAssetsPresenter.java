package de.neoskop.magnolia.dependencymanager.presenter;

import com.google.common.collect.Lists;
import de.neoskop.magnolia.dependencymanager.services.OrphanedAssetsService;
import de.neoskop.magnolia.dependencymanager.util.AssetHolder;
import de.neoskop.magnolia.dependencymanager.views.OrphanedAssetsView;
import info.magnolia.cms.beans.config.MIMEMapping;
import info.magnolia.commands.CommandsManager;
import info.magnolia.commands.chain.Command;
import info.magnolia.commands.impl.MarkNodeAsDeletedCommand;
import info.magnolia.context.Context;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.AssetProvider.AssetNotFoundException;
import info.magnolia.dam.api.AssetProviderRegistry;
import info.magnolia.dam.api.ItemKey;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.dam.jcr.JcrAsset;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.location.LocationController;
import info.magnolia.ui.api.overlay.ConfirmationCallback;
import info.magnolia.ui.contentapp.detail.DetailLocation;
import info.magnolia.ui.contentapp.detail.DetailView;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrphanedAssetsPresenter implements OrphanedAssetsView.Listener {
  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AssetProviderRegistry assetProviderRegistry;
  private final LocationController locationController;
  private OrphanedAssetsView view;
  private final OrphanedAssetsService orphanedAssetsService;
  private Set<ItemKey> selectedAssetUuids;
  private String filter;
  private SubAppContext subAppContext;
  private SimpleTranslator i18n;

  @Inject
  public OrphanedAssetsPresenter(
      OrphanedAssetsView view,
      OrphanedAssetsService orphanedAssetsService,
      AssetProviderRegistry assetProviderRegistry,
      LocationController locationController,
      SubAppContext subAppContext,
      SimpleTranslator i18n) {
    this.view = view;
    this.orphanedAssetsService = orphanedAssetsService;
    this.assetProviderRegistry = assetProviderRegistry;
    this.locationController = locationController;
    this.subAppContext = subAppContext;
    this.i18n = i18n;
  }

  public OrphanedAssetsView start() {
    view.setListener(this);
    refresh();
    return view;
  }

  private void refresh() {
    List<Asset> orphanedAssets = Lists.newArrayList();

    try {
      Set<AssetHolder> holders = orphanedAssetsService.retrieveOrphanedAssets(filter);
      orphanedAssets.addAll(
          holders.stream().map(AssetHolder::getAsset).collect(Collectors.toList()));
    } catch (RepositoryException e) {
      e.printStackTrace();
    }

    view.refresh(orphanedAssets);
  }

  @Override
  public void onItemSelection(Set<ItemKey> assetUuids) {
    selectedAssetUuids = assetUuids;
  }

  @Override
  public void onMarkDelete() {

    subAppContext.openConfirmation(
        MessageStyleTypeEnum.WARNING,
        i18n.translate("orphanedassets.app.actions.confirmRemove.notifications.title"),
        i18n.translate("orphanedassets.app.actions.confirmRemove.notifications.body"),
        i18n.translate("orphanedassets.app.actions.confirmRemove.notifications.confirmButton"),
        i18n.translate("orphanedassets.app.actions.confirmRemove.notifications.cancelButton"),
        true,
        new ConfirmationCallback() {

          @Override
          public void onSuccess() {
            try {
              CommandsManager cm = Components.getComponent(CommandsManager.class);
              Command command = cm.getCommand("markAsDeleted");

              final Map<String, Object> params = new HashMap<>();
              params.put(Context.ATTRIBUTE_REPOSITORY, DamConstants.WORKSPACE);

              for (ItemKey ik : selectedAssetUuids) {
                Asset asset = assetProviderRegistry.getProviderFor(ik).getAsset(ik);

                if (asset instanceof JcrAsset
                    && ((JcrAsset) asset).getProperty("mgnl:deleted") == null) {
                  params.put(Context.ATTRIBUTE_PATH, asset.getParent().getPath());
                  params.put(MarkNodeAsDeletedCommand.DELETED_NODE_PROP_NAME, asset.getName());

                  cm.executeCommand(command, params);
                }
              }

              refresh();
              subAppContext.openNotification(
                  MessageStyleTypeEnum.INFO,
                  true,
                  i18n.translate(
                      "orphanedassets.app.actions.remove.notifications.success.message"));
            } catch (Exception e) {
              subAppContext.openNotification(
                  MessageStyleTypeEnum.ERROR,
                  true,
                  i18n.translate("orphanedassets.app.actions.remove.notifications.error.message"));
              log.error("An error occured on deleting orphaned assets", e);
            }
          }

          @Override
          public void onCancel() {}
        });
  }

  @Override
  public void onEdit() {
    if (selectedAssetUuids.size() != 1) {
      return;
    }

    ItemKey itemKey = selectedAssetUuids.iterator().next();
    Asset asset = assetProviderRegistry.getProviderFor(itemKey).getAsset(itemKey);
    locationController.goTo(
        new DetailLocation("assets", "detail", DetailView.ViewType.EDIT, asset.getPath(), ""));
  }

  @Override
  public void onRefresh() {
    refresh();
  }

  @Override
  public String getIcon(ItemKey itemKey) {
    try {
      Asset asset = assetProviderRegistry.getProviderFor(itemKey).getAsset(itemKey);
      String mimeType = asset.getMimeType();
      String icon = MIMEMapping.DEFAULT_ICON_STYLE;

      if (asset instanceof JcrAsset) {
        try {
          Node node = ((JcrAsset) asset).getNode();

          if (node.isNodeType(NodeTypes.Deleted.NAME)) {
            return "icon-trash";
          }

        } catch (RepositoryException e) {
          return icon;
        }
      }

      if (!StringUtils.isBlank(mimeType)) {
        icon = MIMEMapping.getMIMETypeIconStyle(mimeType);
      }

      return icon;
    } catch (AssetNotFoundException e) {
      return "icon-trash";
    }
  }

  @Override
  public void onSearch(String filter) {
    this.filter = filter;
    refresh();
  }

  @Override
  public void onPublishDeletion() {
    CommandsManager cm = Components.getComponent(CommandsManager.class);
    Command command = cm.getCommand("activate");
    Map<String, Object> params = new HashMap<>();

    params.put("repository", DamConstants.WORKSPACE);
    params.put("recursive", false);

    try {
      for (ItemKey ik : selectedAssetUuids) {
        Asset asset = assetProviderRegistry.getProviderFor(ik).getAsset(ik);

        if (asset instanceof JcrAsset && ((JcrAsset) asset).getProperty("mgnl:deleted") != null) {
          params.put("path", asset.getPath());
          cm.executeCommand(command, params);
        }
      }

      refresh();
      subAppContext.openNotification(
          MessageStyleTypeEnum.INFO,
          true,
          i18n.translate(
              "orphanedassets.app.actions.publishDeletion.notifications.success.message"));
    } catch (Exception e) {
      subAppContext.openNotification(
          MessageStyleTypeEnum.ERROR,
          true,
          i18n.translate("orphanedassets.app.actions.publishDeletion.notifications.error.message"));
      log.error("An error occured on publishing deleted orphaned assets", e);
    }
  }
}
