package de.neoskop.magnolia.dependencymanager.actions;

import de.neoskop.magnolia.dependencymanager.services.ContentDependencyService;
import info.magnolia.commands.CommandsManager;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.AdmincentralEventBus;
import info.magnolia.ui.api.overlay.ConfirmationCallback;
import info.magnolia.ui.framework.action.ActivationAction;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import java.text.MessageFormat;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivationWithAssetsAction<D extends ActivationWithAssetsActionDefinition>
    extends ActivationAction<D> {

  private static final Logger log = LoggerFactory.getLogger(ActivationWithAssetsAction.class);

  private JcrItemAdapter item;
  private final UiContext uiContext;
  private ContentDependencyService contentDependencyService;
  private SimpleTranslator i18n;

  @Inject
  public ActivationWithAssetsAction(
      final D definition,
      final JcrItemAdapter item,
      final CommandsManager commandsManager,
      @Named(AdmincentralEventBus.NAME) EventBus admincentralEventBus,
      SubAppContext uiContext,
      final SimpleTranslator i18n,
      ContentDependencyService contentDependencyService) {

    super(definition, item, commandsManager, admincentralEventBus, uiContext, i18n);

    this.uiContext = uiContext;
    this.item = item;
    this.contentDependencyService = contentDependencyService;
    this.i18n = i18n;
  }

  @Override
  public void execute() throws ActionExecutionException {
    try {
      Set<Node> assets =
          contentDependencyService.retrieveUnpublishedAssets(
              (Node) item.getJcrItem(), getDefinition().isRecursive());

      if (assets.size() > 0) {
        String bodyMessage =
            i18n.translate("pages.browser.actions.activationWithAssets.notifications.body");
        String list = StringUtils.EMPTY;
        for (Node asset : assets) {
          list += "<li>" + asset.getPath() + "</li>";
        }

        uiContext.openConfirmation(
            MessageStyleTypeEnum.WARNING,
            i18n.translate("pages.browser.actions.activationWithAssets.notifications.title"),
            MessageFormat.format(bodyMessage, list),
            i18n.translate("pages.browser.actions.activationWithAssets.buttons.confirm.label"),
            i18n.translate("pages.browser.activationWithAssets.buttons.cancel.label"),
            false,
            new ConfirmationCallback() {

              @Override
              public void onSuccess() {
                try {
                  // Assets müssen noch mal geholt werden, da
                  // sonst die Sessions der Assets nicht mehr
                  // gueltig sind und Exceptions auftreten
                  Set<Node> assets =
                      contentDependencyService.retrieveUnpublishedAssets(
                          (Node) item.getJcrItem(), getDefinition().isRecursive());
                  Set<Node> unpublishedAssetParents =
                      contentDependencyService.retrieveUnpublishedParents(assets);

                  boolean recursive = getDefinition().isRecursive();
                  getDefinition().setRecursive(false);
                  for (Node unpublishedAssetParent : unpublishedAssetParents) {
                    setCurrentItem(new JcrNodeAdapter(unpublishedAssetParent));
                    executeOnItem(new JcrNodeAdapter(unpublishedAssetParent));
                  }

                  for (Node asset : assets) {
                    setCurrentItem(new JcrNodeAdapter(asset));
                    executeOnItem(new JcrNodeAdapter(asset));
                  }

                  getDefinition().setRecursive(recursive);
                  publishPage();
                } catch (Exception e) {
                  log.error("An error occurred on publishing content", e);
                  openErrorNotification();
                }
              }

              @Override
              public void onCancel() {}
            });
      } else {
        publishPage();
      }
    } catch (Exception ex) {
      log.error("An error occurred on publishing content", ex);
      openErrorNotification();
    }
  }

  private void publishPage() throws ActionExecutionException {
    setCurrentItem(item);
    executeOnItem(item);

    String message = getSuccessMessage();
    if (StringUtils.isNotBlank(message)) {
      uiContext.openNotification(MessageStyleTypeEnum.INFO, true, message);
    }
  }

  private void openErrorNotification() {
    String message = getErrorNotification();
    if (StringUtils.isNotBlank(message)) {
      uiContext.openNotification(MessageStyleTypeEnum.ERROR, false, message);
    }
  }
}
