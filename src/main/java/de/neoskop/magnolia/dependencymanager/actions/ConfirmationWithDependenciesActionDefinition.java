package de.neoskop.magnolia.dependencymanager.actions;

import info.magnolia.ui.framework.action.ConfirmationActionDefinition;

public class ConfirmationWithDependenciesActionDefinition extends ConfirmationActionDefinition {
  public ConfirmationWithDependenciesActionDefinition() {
    setImplementationClass(ConfirmationWithDependenciesAction.class);
  }
}
