package de.neoskop.magnolia.dependencymanager.actions;

import de.neoskop.magnolia.dependencymanager.services.ContentDependencyService;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.app.AppContext;
import info.magnolia.ui.api.location.LocationController;
import info.magnolia.ui.contentapp.detail.DetailLocation;
import info.magnolia.ui.contentapp.detail.DetailView;
import info.magnolia.ui.vaadin.integration.jcr.AbstractJcrNodeAdapter;
import javax.inject.Inject;
import javax.jcr.RepositoryException;

public class DependenciesViewAction extends AbstractAction<DependenciesViewActionDefinition> {
  private AbstractJcrNodeAdapter node;
  private LocationController locationController;
  private final AppContext appContext;

  @Inject
  public DependenciesViewAction(
      final DependenciesViewActionDefinition definition,
      AbstractJcrNodeAdapter node,
      LocationController locationController,
      AppContext appContext,
      ContentDependencyService contentDependencyService) {
    super(definition);
    this.node = node;
    this.locationController = locationController;
    this.appContext = appContext;
  }

  @Override
  public void execute() throws ActionExecutionException {
    try {
      DetailLocation location =
          new DetailLocation(
              appContext.getName(),
              "dependencies",
              DetailView.ViewType.VIEW,
              node.getJcrItem().getPath(),
              "");
      locationController.goTo(location);
    } catch (RepositoryException e) {
      throw new ActionExecutionException(e);
    }
  }
}
