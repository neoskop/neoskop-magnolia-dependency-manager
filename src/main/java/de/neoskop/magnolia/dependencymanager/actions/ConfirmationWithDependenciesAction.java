package de.neoskop.magnolia.dependencymanager.actions;

import com.google.common.collect.Sets;
import com.vaadin.v7.data.Item;
import de.neoskop.magnolia.dependencymanager.services.ContentDependencyService;
import de.neoskop.magnolia.dependencymanager.util.NodeHolder;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.action.ActionExecutor;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.framework.action.ConfirmationAction;
import info.magnolia.ui.framework.action.ConfirmationActionDefinition;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

public class ConfirmationWithDependenciesAction extends ConfirmationAction {
  private ContentDependencyService contentDependencyService;
  private SimpleTranslator i18n;
  private List<? extends Item> items;

  public ConfirmationWithDependenciesAction(
      ConfirmationActionDefinition definition,
      Item item,
      UiContext uiContext,
      ActionExecutor actionExecutor,
      SimpleTranslator i18n,
      ContentConnector contentConnector,
      ContentDependencyService contentDependencyService) {
    super(definition, item, uiContext, actionExecutor, i18n, contentConnector);
    this.contentDependencyService = contentDependencyService;
  }

  @Inject
  public ConfirmationWithDependenciesAction(
      ConfirmationActionDefinition definition,
      List<? extends Item> items,
      UiContext uiContext,
      ActionExecutor actionExecutor,
      SimpleTranslator i18n,
      ContentConnector contentConnector,
      ContentDependencyService contentDependencyService) {
    super(definition, items, uiContext, actionExecutor, i18n, contentConnector);
    this.items = items;
    this.i18n = i18n;
    this.contentDependencyService = contentDependencyService;
  }

  @Override
  protected String getConfirmationMessage() throws Exception {
    final StringBuilder confirmationMessageBuilder =
        new StringBuilder(super.getConfirmationMessage());
    Set<NodeHolder> dependentPages = Sets.newHashSet();

    for (Item item : items) {
      if (item instanceof JcrNodeAdapter) {
        Set<NodeHolder> dependentPagesMap =
            contentDependencyService.retrieveDependentPages(((JcrNodeAdapter) item).getJcrItem());
        dependentPages.addAll(dependentPagesMap);
      }
    }

    if (!dependentPages.isEmpty()) {
      confirmationMessageBuilder
          .append(" ")
          .append(i18n.translate("pages.browser.actions.confirmDeletion.dependenciesExist"));
      confirmationMessageBuilder.append("<ul>");
      dependentPages
          .stream()
          .forEach(
              a ->
                  confirmationMessageBuilder
                      .append("<li><pre>")
                      .append(a.getPath())
                      .append("</pre></li>"));
      confirmationMessageBuilder.append("</ul>");
    }

    return confirmationMessageBuilder.toString();
  }
}
