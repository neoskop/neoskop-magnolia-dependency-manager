package de.neoskop.magnolia.dependencymanager.actions;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class DependenciesViewActionDefinition extends ConfiguredActionDefinition {
  public DependenciesViewActionDefinition() {
    setImplementationClass(DependenciesViewAction.class);
  }
}
