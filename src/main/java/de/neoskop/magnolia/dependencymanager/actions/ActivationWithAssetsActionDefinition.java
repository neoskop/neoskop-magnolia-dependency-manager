package de.neoskop.magnolia.dependencymanager.actions;

import info.magnolia.ui.framework.action.ActivationActionDefinition;

public class ActivationWithAssetsActionDefinition extends ActivationActionDefinition {

  public ActivationWithAssetsActionDefinition() {
    setImplementationClass(ActivationWithAssetsAction.class);
  }
}
