package de.neoskop.magnolia.dependencymanager.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.delta.ModuleBootstrapTask;
import info.magnolia.module.delta.Task;
import info.magnolia.module.model.Version;
import java.util.List;

/**
 * This class is optional and lets you manager the versions of your module, by registering "deltas"
 * to maintain the module's configuration, or other type of content. If you don't need this, simply
 * remove the reference to this class in the module descriptor xml.
 */
public class DependencyManagerModuleVersionHandler extends DefaultModuleVersionHandler {
  @Override
  protected List<Task> getDefaultUpdateTasks(Version forVersion) {
    final List<Task> defaultUpdateTasks = super.getDefaultUpdateTasks(forVersion);
    defaultUpdateTasks.add(new ModuleBootstrapTask());
    return defaultUpdateTasks;
  }
}
