package de.neoskop.magnolia.dependencymanager.domain;

import org.apache.commons.io.FileUtils;

public class Filesize implements Comparable<Filesize> {

  private long filesize;

  public Filesize(long filesize) {
    this.filesize = filesize;
  }

  public long getFilesize() {
    return filesize;
  }

  @Override
  public String toString() {
    return FileUtils.byteCountToDisplaySize(filesize);
  }

  @Override
  public int compareTo(Filesize anotherFilesize) {
    return compare(this.getFilesize(), anotherFilesize.getFilesize());
  }

  public static int compare(long x, long y) {
    return (x < y) ? -1 : ((x == y) ? 0 : 1);
  }
}
