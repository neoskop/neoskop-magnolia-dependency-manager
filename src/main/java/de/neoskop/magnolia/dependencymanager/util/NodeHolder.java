package de.neoskop.magnolia.dependencymanager.util;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class NodeHolder {
  private Node node;
  private String path;
  private boolean isPage;

  public NodeHolder(Node node, boolean isPage) throws RepositoryException {
    this.node = node;
    this.isPage = isPage;
    this.path = node.getPath();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(getPath()).build();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    return new EqualsBuilder().append(path, ((NodeHolder) o).getPath()).build();
  }

  public Node getNode() {
    return node;
  }

  @Override
  public String toString() {
    return path;
  }

  public String getPath() {
    return path;
  }

  public boolean isPage() {
    return isPage;
  }

  public boolean isAsset() {
    return !isPage;
  }
}
