package de.neoskop.magnolia.dependencymanager.util;

import info.magnolia.dam.api.Asset;
import org.apache.commons.lang3.builder.CompareToBuilder;

public class AssetHolder implements Comparable<AssetHolder> {
  private Asset asset;

  public AssetHolder(Asset asset) {
    this.setAsset(asset);
  }

  @Override
  public int hashCode() {
    return asset.getItemKey().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    return asset.getItemKey().equals(((AssetHolder) o).getAsset().getItemKey());
  }

  public Asset getAsset() {
    return asset;
  }

  public void setAsset(Asset asset) {
    this.asset = asset;
  }

  @Override
  public String toString() {
    return asset.getName();
  }

  @Override
  public int compareTo(AssetHolder o) {
    return new CompareToBuilder().append(this.asset.getPath(), o.asset.getPath()).toComparison();
  }
}
