package de.neoskop.magnolia.dependencymanager;

import java.util.List;

/**
 * This class is optional and represents the configuration for the ${module-name} module. By
 * exposing simple getter/setter/adder methods, this bean can be configured via content2bean using
 * the properties and node from <tt>config:/modules/${module-name}</tt>. If you don't need this,
 * simply remove the reference to this class in the module descriptor xml.
 */
public class DependencyManagerModule {

  private List<String> orphanedAssetFilters;
  private List<String> additionalAssetRepositories;

  public List<String> getOrphanedAssetFilters() {
    return orphanedAssetFilters;
  }

  public void setOrphanedAssetFilters(List<String> orphanedAssetFilters) {
    this.orphanedAssetFilters = orphanedAssetFilters;
  }

  public List<String> getAdditionalAssetRepositories() {
    return additionalAssetRepositories;
  }

  public void setAdditionalAssetRepositories(List<String> additionalAssetRepositories) {
    this.additionalAssetRepositories = additionalAssetRepositories;
  }
}
