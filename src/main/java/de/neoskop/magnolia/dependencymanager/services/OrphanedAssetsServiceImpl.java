package de.neoskop.magnolia.dependencymanager.services;

import com.google.common.collect.Sets;
import de.neoskop.magnolia.dependencymanager.DependencyManagerModule;
import de.neoskop.magnolia.dependencymanager.util.AssetHolder;
import info.magnolia.cms.util.QueryUtil;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.api.AssetProviderRegistry;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.dam.jcr.JcrAsset;
import info.magnolia.dam.jcr.JcrAssetProvider;
import info.magnolia.repository.RepositoryConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;

public class OrphanedAssetsServiceImpl implements OrphanedAssetsService {
  private final AssetProviderRegistry assetProviderRegistry;
  private List<String> orphanedAssetFilters;
  private List<String> additionalAssetRepositories;

  @Inject
  public OrphanedAssetsServiceImpl(
      AssetProviderRegistry assetProviderRegistry,
      DependencyManagerModule dependencyManagerModule) {
    this.assetProviderRegistry = assetProviderRegistry;
    this.orphanedAssetFilters = dependencyManagerModule.getOrphanedAssetFilters();
    this.additionalAssetRepositories = dependencyManagerModule.getAdditionalAssetRepositories();
  }

  @Override
  public Set<AssetHolder> retrieveOrphanedAssets(String filter) throws RepositoryException {
    Set<AssetHolder> assets = retrieveAllAssets();

    if (StringUtils.isNotBlank(filter)) {
      try {
        final Pattern filterPattern =
            Pattern.compile(".*" + filter + ".*", Pattern.CASE_INSENSITIVE);
        assets =
            assets
                .stream()
                .filter(
                    a ->
                        filterPattern
                            .matcher(a.getAsset().getPath() + " " + a.getAsset().getMimeType())
                            .matches())
                .collect(Collectors.toSet());
      } catch (PatternSyntaxException e) {
        return Sets.newHashSet();
      }
    }

    filterAssetsByPredicates(assets);
    filterUsedAssets(assets);

    return assets;
  }

  private void filterAssetsByPredicates(Set<AssetHolder> assets) {
    List<Predicate<Asset>> predicates = new ArrayList<Predicate<Asset>>();

    if (orphanedAssetFilters != null) {
      orphanedAssetFilters.forEach(
          orphanedAssetFilter -> {
            try {
              Class<?> clazz = Class.forName(orphanedAssetFilter);
              @SuppressWarnings("unchecked")
              Predicate<Asset> predicate = ((Predicate<Asset>) clazz.newInstance());
              predicates.add(predicate);
            } catch (Exception e) {
              e.printStackTrace();
            }
          });
    }

    Set<AssetHolder> predicateFailedAssets =
        assets
            .stream()
            .filter(assetHolder -> !testPredicates(assetHolder.getAsset(), predicates))
            .collect(Collectors.toSet());
    predicateFailedAssets.forEach(asset -> assets.remove(asset));
  }

  private boolean testPredicates(Asset asset, List<Predicate<Asset>> predicates) {
    for (Predicate<Asset> predicate : predicates) {
      if (!predicate.test(asset)) {
        return false;
      }
    }

    return true;
  }

  private void filterUsedAssets(Set<AssetHolder> assets) throws RepositoryException {
    for (Iterator<AssetHolder> iterator = assets.iterator(); iterator.hasNext(); ) {
      AssetHolder asset = iterator.next();
      String uuid = asset.getAsset().getItemKey().getAssetId();
      String query = "SELECT * FROM [nt:base] AS node WHERE CONTAINS(node.*, '" + uuid + "')";
      NodeIterator nodes =
          QueryUtil.search(RepositoryConstants.WEBSITE, query, "JCR-SQL2", "nt:base");
      boolean removeAsset = false;

      if (nodes.hasNext()) {
        removeAsset = true;
      }

      if (additionalAssetRepositories != null && additionalAssetRepositories.size() > 0) {
        for (String additionalRepo : additionalAssetRepositories) {
          NodeIterator additionalNodes =
              QueryUtil.search(additionalRepo, query, "JCR-SQL2", "nt:base");
          if (additionalNodes.hasNext()) {
            removeAsset = true;
          }
        }
      }

      if (removeAsset) {
        iterator.remove();
      }
    }
  }

  private Set<AssetHolder> retrieveAllAssets() throws RepositoryException {
    Set<AssetHolder> result = new TreeSet<>();
    String query = "SELECT * FROM [mgnl:asset]";
    NodeIterator nodes = QueryUtil.search(DamConstants.WORKSPACE, query, "JCR-SQL2", "mgnl:asset");

    while (nodes.hasNext()) {
      Node node = nodes.nextNode();
      JcrAssetProvider assetProvider =
          (JcrAssetProvider) assetProviderRegistry.getProviderById("jcr");
      result.add(new AssetHolder(new JcrAsset(assetProvider, node)));
    }

    return result;
  }
}
