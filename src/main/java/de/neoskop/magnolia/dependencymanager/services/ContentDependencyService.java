package de.neoskop.magnolia.dependencymanager.services;

import de.neoskop.magnolia.dependencymanager.util.NodeHolder;
import java.util.Map;
import java.util.Set;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Service to retrieve resources (pages and assets) that depend on other resources
 *
 * @author Arne Diekmann, Christian Binzer
 * @since 1.0.0
 */
public interface ContentDependencyService {
  /**
   * Retrieves all assets and pages the given page or any child nodes depend upon
   *
   * @param nodePath The path of the node to retrieve all dependencies for
   * @param includeSubpages if true all dependencies of sub pages are gathered as well
   * @return A map associating dependencies with a set of pages (which reference them)
   * @throws RepositoryException if the given node could not be found or no adjacent node with type
   *     mgnl:page could be found
   */
  Map<NodeHolder, Set<NodeHolder>> retrieveDependentResourcesOfPage(
      String nodePath, boolean includeSubpages) throws RepositoryException;

  /**
   * Retrieves all pages the given asset depends upon
   *
   * @param nodePath The path of the node to retrieve all dependencies for
   * @return a set of dependent pages
   * @throws RepositoryException if the given node could not be found or execution of the JCR-SQL
   *     query failed
   */
  Set<NodeHolder> retrieveDependentResourcesOfAsset(String nodePath) throws RepositoryException;

  /**
   * Retrieves all pages that link to <code>node</code>.
   *
   * @param node the node of the resource to check links for
   * @return a set of page nodes (wrapped as {@link NodeHolder})
   * @throws RepositoryException when executing JCR-SQL query fails
   */
  Set<NodeHolder> retrieveDependentPages(Node node) throws RepositoryException;

  /**
   * Retrieves all unpublished assets in the <code>node</code>.
   *
   * @param node the node to retrieve all unpublished assets from
   * @param includeSubpages if true you get the unpublished assets from the subpages
   * @return a set of asset nodes
   * @throws RepositoryException when executing JCR-SQL query fails
   */
  Set<Node> retrieveUnpublishedAssets(Node node, boolean includeSubpages)
      throws RepositoryException;

  /**
   * Retrieves all unpublished parents for the <code>nodes</code>.
   *
   * @param nodes the nodes to retrieve all unpublished parents
   * @return a set of nodes
   * @throws RepositoryException when executing JCR-SQL query fails
   */
  Set<Node> retrieveUnpublishedParents(Iterable<Node> nodes) throws RepositoryException;
}
