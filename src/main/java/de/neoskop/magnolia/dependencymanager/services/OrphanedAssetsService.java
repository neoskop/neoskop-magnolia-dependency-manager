package de.neoskop.magnolia.dependencymanager.services;

import de.neoskop.magnolia.dependencymanager.util.AssetHolder;
import java.util.Set;
import javax.jcr.RepositoryException;

/**
 * Service to retrieve assets which are not referenced by any page
 *
 * @author Arne Diekmann
 * @since 1.0.3
 */
public interface OrphanedAssetsService {
  /**
   * Retrieves assets which are not linked by any page
   *
   * @param filter a RegEx pattern to filter path for
   * @return a set of assets (wrapped as {@link AssetHolder})
   * @throws RepositoryException when executing of any JCR-SQL query fails
   */
  Set<AssetHolder> retrieveOrphanedAssets(String filter) throws RepositoryException;
}
