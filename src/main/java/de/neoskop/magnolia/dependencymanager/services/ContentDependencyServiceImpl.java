package de.neoskop.magnolia.dependencymanager.services;

import com.google.common.collect.Sets;
import de.neoskop.magnolia.dependencymanager.util.NodeHolder;
import info.magnolia.cms.util.QueryUtil;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.repository.RepositoryConstants;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;
import info.magnolia.ui.vaadin.integration.contentconnector.JcrContentConnector;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentDependencyServiceImpl implements ContentDependencyService {

  private static final Logger log = LoggerFactory.getLogger(ContentDependencyServiceImpl.class);

  private final JcrContentConnector contentConnector;

  @Inject
  public ContentDependencyServiceImpl(ContentConnector contentConnector) {
    this.contentConnector = (JcrContentConnector) contentConnector;
  }

  public Map<NodeHolder, Set<NodeHolder>> retrieveDependentResourcesOfPage(
      String nodePath, boolean includeSubpages) throws RepositoryException {
    Node node = retrieveNodeFromPath(nodePath);
    Set<NodeHolder> pages = Sets.newHashSet(new NodeHolder(node, true));
    Map<NodeHolder, Set<NodeHolder>> result = new HashMap<>();

    if (includeSubpages) {
      pages.addAll(retrieveSubpagesOf(node));
    }

    for (NodeHolder page : pages) {
      Set<NodeHolder> dependentPages = retrieveDependentPages(page.getNode());

      dependentPages.forEach(
          dep -> {
            if (result.containsKey(dep)) {
              result.get(dep).add(page);
            } else {
              result.put(dep, Sets.newHashSet(page));
            }
          });
    }

    return result;
  }

  private Set<NodeHolder> retrieveSubpagesOf(Node node) throws RepositoryException {
    Set<NodeHolder> descendants = retrieveAllDescendants(node);
    return descendants.stream().filter(d -> d.isPage()).collect(Collectors.toSet());
  }

  public Set<NodeHolder> retrieveDependentResourcesOfAsset(String nodePath)
      throws RepositoryException {
    Node node = retrieveNodeFromPath(nodePath);
    return retrieveDependentPages(node);
  }

  private Node retrieveNodeFromPath(String pageNodePath) throws RepositoryException {
    Object itemId = contentConnector.getItemIdByUrlFragment(pageNodePath);
    return (Node) contentConnector.getItem(itemId).getJcrItem();
  }

  @Override
  public Set<NodeHolder> retrieveDependentPages(Node node) throws RepositoryException {
    Set<NodeHolder> result = Sets.newHashSet();
    String uuid = node.getProperty("jcr:uuid").getString();

    String query = "SELECT * FROM [nt:base] AS node WHERE CONTAINS(node.*, '" + uuid + "')";
    NodeIterator nodes =
        QueryUtil.search(RepositoryConstants.WEBSITE, query, "JCR-SQL2", "nt:base");

    while (nodes.hasNext()) {
      Node parentPage = findNearestPageOfNode(nodes.nextNode());

      if (!parentPage.getPath().equals(node.getPath())) {
        result.add(new NodeHolder(parentPage, true));
      }
    }

    return result;
  }

  private Set<NodeHolder> retrieveAllDescendants(Node parent) throws RepositoryException {
    HashSet<NodeHolder> result = Sets.newHashSet();
    NodeIterator nodeIterator = parent.getNodes();

    while (nodeIterator.hasNext()) {
      Node childNode = nodeIterator.nextNode();
      result.add(new NodeHolder(childNode, childNode.isNodeType(NodeTypes.Page.NAME)));
      result.addAll(retrieveAllDescendants(childNode));
    }

    return result;
  }

  private Node findNearestPageOfNode(Node node) throws RepositoryException {
    Node currentNode = node;

    while (currentNode != null) {
      if (currentNode.isNodeType(NodeTypes.Page.NAME)) {
        return currentNode;
      }

      currentNode = currentNode.getParent();
    }

    throw new ItemNotFoundException();
  }

  @Override
  public Set<Node> retrieveUnpublishedAssets(Node node, boolean includeSubpages)
      throws RepositoryException {
    Set<Node> result = Sets.newHashSet();
    Set<Node> allUnpublishedAssets = retrieveAllUnpublishedAssets();

    for (Node unpublishedAsset : allUnpublishedAssets) {
      if (containsUUID(node, unpublishedAsset.getIdentifier(), includeSubpages)) {
        result.add(unpublishedAsset);
      }
    }

    return result;
  }

  private Set<Node> retrieveAllUnpublishedAssets() throws RepositoryException {
    Set<Node> result = Sets.newHashSet();
    String query = "SELECT * FROM [mgnl:asset]";
    NodeIterator nodes = QueryUtil.search(DamConstants.WORKSPACE, query);

    while (nodes.hasNext()) {
      Node asset = nodes.nextNode();

      if (!isPublished(asset)) {
        result.add(asset);
      }
    }

    return result;
  }

  private boolean containsUUID(Node node, String uuid, boolean checkSubpages)
      throws RepositoryException {
    PropertyIterator properties = node.getProperties();
    while (properties.hasNext()) {
      Property property = properties.nextProperty();
      if (property.getType() == PropertyType.STRING) {
        String value = null;
        if (property.isMultiple()) {
          value = StringUtils.EMPTY;
          Value[] values = property.getValues();

          for (Value v : values) {
            if (v.getType() == PropertyType.STRING) {
              value += v.getString() + " ";
            }
          }
        } else {
          value = property.getString();
        }

        if (value != null && value.contains(uuid)) {
          return true;
        }
      }
    }

    NodeIterator nodes = node.getNodes();
    while (nodes.hasNext()) {
      Node subNode = nodes.nextNode();
      if (checkSubpages || !subNode.getPrimaryNodeType().getName().equals(NodeTypes.Page.NAME)) {
        if (containsUUID(subNode, uuid, checkSubpages)) {
          return true;
        }
      }
    }

    return false;
  }

  private boolean isPublished(Node node) throws RepositoryException {
    Integer status = NodeTypes.Activatable.getActivationStatus(node);

    if (status.equals(NodeTypes.Activatable.ACTIVATION_STATUS_MODIFIED)
        || status.equals(NodeTypes.Activatable.ACTIVATION_STATUS_NOT_ACTIVATED)) {
      return false;
    }

    return true;
  }

  @Override
  public Set<Node> retrieveUnpublishedParents(Iterable<Node> nodes) throws RepositoryException {
    Set<Node> unpublishedParents =
        Sets.newTreeSet(
            new Comparator<Node>() {

              @Override
              public int compare(Node node1, Node node2) {
                try {
                  if (node1.getDepth() < node2.getDepth()) {
                    return -1;
                  } else if (node1.getDepth() > node2.getDepth()) {
                    return 1;
                  }
                } catch (RepositoryException e) {
                  e.printStackTrace();
                }

                return 0;
              }
            });

    for (Node node : nodes) {
      Set<Node> unpublishedParentsAsNodes = retrieveUnpublishedParents(node);
      for (Node unpublishedParent : unpublishedParentsAsNodes) {
        if (!containsNode(unpublishedParents.stream(), unpublishedParent)) {
          unpublishedParents.add(unpublishedParent);
        }
      }
    }

    return unpublishedParents;
  }

  private boolean containsNode(Stream<Node> nodes, Node node) {
    return nodes.filter(n -> nodeEquals(n, node)).findFirst().isPresent();
  }

  private boolean nodeEquals(Node node1, Node node2) {
    try {
      return node1.getIdentifier().equals(node2.getIdentifier());
    } catch (RepositoryException e) {
      log.error("An error occurred on checking node equality", e);
    }

    return false;
  }

  private Set<Node> retrieveUnpublishedParents(Node asset) {
    Set<Node> unpublishedParents = Sets.newHashSet();

    try {
      Node unpublishedParent = retrieveUnpublishedParent(asset);
      while (unpublishedParent != null) {
        unpublishedParents.add(unpublishedParent);
        unpublishedParent = retrieveUnpublishedParent(unpublishedParent);
      }
    } catch (ItemNotFoundException e) {
    } catch (RepositoryException e) {
      log.error("An error occurred on retrieving unpublished parents", e);
    }

    return unpublishedParents;
  }

  private Node retrieveUnpublishedParent(Node node) throws RepositoryException {
    Node parent = node.getParent();
    if (!parent.getPath().equals("/") && !isPublished(parent)) {
      return parent;
    }

    return null;
  }
}
