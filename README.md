# README

Allgemeine Beschreibung vom Modul **Dependency Manager**.

# Abhängigkeiten

Bis Version 1.5.3:

- [Magnolia CMS][1] >= 5.3.7
- [Magnolia DAM Module][4] >= 2.0.4

Ab Version 1.6.0:

- [Magnolia CMS][1] >= 5.6.7
- [Magnolia DAM Module][4] >= 2.3.2

# Installation

Als Repository muss [JitPack](https://jitpack.io/) in der `pom.xml` des Magnolia-Projektes hinzugefügt werden:

```xml
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Das Modul muss dann in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-dependency-manager</artifactId>
	<version>1.5.3</version>
</dependency>
```

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
[3]: http://maven.apache.org/maven-release/maven-release-plugin/
[4]: https://documentation.magnolia-cms.com/display/DOCS/Digital+Asset+Management+module
